import unittest
import json
from unittest.mock import MagicMock
from src.ev_data_retrieval import save_and_upload_partitions

class TestSaveAndUploadPartitions(unittest.TestCase):

    def setUp(self):
        self.partitions = {
            'WA': {
                'Seattle': {
                    2020: [['Sample', 'Data', '2020']],
                    2021: [['Sample', 'Data', '2021']]
                }
            }
        }
        self.minio_client = MagicMock()
        self.bucket_name = "test-bucket"
        # Simulate bucket does not exist
        self.minio_client.bucket_exists.return_value = False

    def test_save_and_upload(self):
        save_and_upload_partitions(self.partitions, self.minio_client, self.bucket_name)

        # Check if the MinIO client's methods were called correctly
        self.assertTrue(self.minio_client.make_bucket.called)
        self.assertEqual(self.minio_client.fput_object.call_count, 2)  # Two calls for two model years

    def tearDown(self):
        # Cleanup any created files/directories
        import shutil
        shutil.rmtree('data/WA', ignore_errors=True)

if __name__ == '__main__':
    unittest.main()
