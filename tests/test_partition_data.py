import unittest
import json
from src.ev_data_retrieval import partition_data

class TestPartitionData(unittest.TestCase):

    def setUp(self):
        # Create a sample data file
        self.sample_data_file = 'data/sample_data.json'
        sample_data = {
            'data': [
                [1, 2, 'King', 4, 5, 6, 7, 8, 9, 'Seattle', 10, 'WA', 12, 2020],
                [1, 2, 'King', 4, 5, 6, 7, 8, 9, 'Seattle', 10, 'WA', 12, 2021]
            ]
        }
        with open(self.sample_data_file, 'w') as file:
            json.dump(sample_data, file)

    def test_partition_data_structure(self):
        partitions = partition_data(self.sample_data_file)
        self.assertIn('WA', partitions)
        self.assertIn('Seattle', partitions['WA'])
        self.assertIn(2020, partitions['WA']['Seattle'])
        self.assertIn(2021, partitions['WA']['Seattle'])

    def tearDown(self):
        # Cleanup the sample data file
        import os
        os.remove(self.sample_data_file)

if __name__ == '__main__':
    unittest.main()
