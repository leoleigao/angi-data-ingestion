import json
import requests
from collections import defaultdict
from minio import Minio
from minio.error import S3Error
import os

def fetch_data(api_url):
    # Ensure the data directory exists
    if not os.path.exists('data'):
        os.makedirs('data')

    response = requests.get(api_url, stream=True)
    if response.status_code == 200:
        with open('data/electric_vehicle_data.json', 'wb') as file:
            for data in response.iter_content(1024):
                file.write(data)
        print("Data download successful.")
        return True
    else:
        print("Failed to fetch data")
        return False

def partition_data(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)

    records = data.get('data', [])

    partitions = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    for record in records:
        state = record[11]  # State field index
        county = record[9]  # County field index
        model_year = record[13]  # Model Year field index
        partitions[state][county][model_year].append(record)

    return partitions

def save_and_upload_partitions(partitions, minio_client, bucket_name):
    for state, counties in partitions.items():
        for county, model_years in counties.items():
            for model_year, records in model_years.items():
                # Construct the object name based on State/County/ModelYear hierarchy
                directory = f"data/{state}/{county}"
                # Ensure the directory structure exists
                if not os.path.exists(directory):
                    os.makedirs(directory)
                object_name = f"{directory}/{model_year}.json"
                # Saving the file locally before upload
                with open(object_name, 'w') as file:
                    json.dump(records, file)
                # Upload the file
                upload_to_minio(object_name, bucket_name, minio_client)

def upload_to_minio(object_name, bucket_name, minio_client):
    try:
        # Check if the bucket exists
        if not minio_client.bucket_exists(bucket_name):
            minio_client.make_bucket(bucket_name)
        # Upload the file
        minio_client.fput_object(bucket_name, object_name, object_name)
        print(f"File '{object_name}' uploaded to bucket '{bucket_name}'")
    except S3Error as exc:
        print("Error occurred: ", exc)

def main():
    api_url = "https://data.wa.gov/api/views/f6w7-q2d2/rows.json?accessType=DOWNLOAD"
    if fetch_data(api_url):
        partitions = partition_data('data/electric_vehicle_data.json')
        minio_client = Minio("my-minio.minio:9000",
                             access_key="5bMJTtrGs1hyQz7q9hdK",
                             secret_key="YcljBkkqLIFefQT4KzbxXNHsrtmPeODq7TEQOokM",
                             secure=False)
        save_and_upload_partitions(partitions, minio_client, "ev-population")

if __name__ == "__main__":
    main()
