# tests/test_data_fetch.py

import unittest
from src.ev_data_retrieval import fetch_data

class TestDataFetch(unittest.TestCase):

    def test_fetch_data_success(self):
        api_url = "https://data.wa.gov/api/views/f6w7-q2d2/rows.json?accessType=DOWNLOAD"
        self.assertTrue(fetch_data(api_url))

if __name__ == '__main__':
    unittest.main()

