
# Data Export Pipeline Documentation

This document outlines the steps for setting up a data export pipeline using Kubernetes, Minio, and GitLab CI/CD.

## Setting Up the Minikube Cluster

### Prerequisites
- Docker Desktop installed on a MacBook Air with M1 chip.
- kubectl (Kubernetes command-line tool) installed.

### Installation Steps
1. **Install Minikube**:
   ```bash
   brew install minikube
   ```
2. **Start Minikube**:
   ```bash
   minikube start
   ```
3. **Enable Minikube Add-ons**:
   ```bash
   minikube addons enable ingress
   ```

## Deploying Minio on Kubernetes

### Steps
1. **Add Minio Helm Chart Repository**:
   ```bash
   helm repo add minio https://charts.min.io/
   ```
2. **Install Minio**:
   ```bash
   helm install my-minio minio/minio --set resources.requests.memory=1Gi,mode=standalone,replicas=2 --namespace minio --create-namespace
   ```
3. **Port Forwarding for Minio Services**:
   - To access the Minio console, set up port forwarding for the Minio console service:
     ```bash
     kubectl port-forward service/my-minio-console -n minio 9001:9001
     ```
   - Similarly, for the Minio service itself:
     ```bash
     kubectl port-forward service/my-minio -n minio 9000:9000
     ```

## Running the Data Export Application

### Steps
1. **Set Up the Python Environment**:
   - Create and activate a virtual environment:
     ```bash
     python3 -m venv venv
     source venv/bin/activate
     ```
   - Install dependencies:
     ```bash
     pip install -r requirements.txt
     ```
2. **Run the Application**:
   ```bash
   python src/ev_data_retrieval.py
   ```

## Configuring GitLab CI/CD Pipeline

### Prerequisites
- A GitLab repository for the project.

### Configuration Steps
1. **CI/CD File**: Ensure `.gitlab-ci.yml` is set up in your repository.
2. **Dockerfile**: Verify it's configured to build your application image.
3. **Kubernetes Configuration**: Check `ev-data-ingestion-deployment.yaml` for accuracy.
4. **Push to GitLab**: Commit and push your changes.
5. **Monitor Pipeline**: Observe the pipeline’s progress in GitLab's CI/CD section.
6. **Deployment**: The `deploy_job` in the pipeline handles deployment to Minikube.

### Notes
- Regularly check the status of Kubernetes resources with `kubectl`.
- Securely store sensitive credentials outside of scripts and YAML files.
- Regularly update dependencies for enhanced security and functionality.
